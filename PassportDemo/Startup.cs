﻿using Microsoft.Owin;
using Owin;
using PassportDemo;

[assembly: OwinStartup(typeof (Startup))]

namespace PassportDemo
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}