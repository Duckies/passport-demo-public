﻿var errorViewModel = function(id, value, factoryNodeId, parent) {
    var self = this;
    self.id = ko.observable(id);
    self.value = ko.observable(value);
    self.factoryNodeId = ko.observable(factoryNodeId);
    self.parent = parent;

    self.removeFromParent = function() {
        parent.removeError(self.id());
    };
}