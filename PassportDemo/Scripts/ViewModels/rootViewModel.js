﻿var rootViewModel = function(tree) {
    var self = this;
    self.factories = ko.observableArray();
    self.selectedItem = ko.observable(factoryViewModel);
    self.tree = tree;
    self.errorMessages = ko.observableArray();

    self.addNode = function() {
        //refuctor this so that these are parameters passed in.
        self.tree.server.addNode(self.selectedItem().factoryName(), self.selectedItem().lowerBound(), self.selectedItem().upperBound());
    };
    self.setSelectedItem = function(id) {
        var selected = ko.utils.arrayFirst(self.factories(), function(node) {
            return node.id() == id;
        });
        if (!selected)
            selected = new factoryViewModel("", "", "", "", null, self);
        else
            selected = new factoryViewModel(
                selected.id(),
                selected.factoryName(),
                selected.lowerBound(),
                selected.upperBound(),
                null,
                self
            );
        this.selectedItem(selected);
    };
    self.removeError = function(errorId) {
        self.errorMessages.remove(function(node) {
            return node.id() == errorId;
        });
    };
}