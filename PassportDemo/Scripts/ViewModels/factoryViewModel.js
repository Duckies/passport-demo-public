﻿var factoryViewModel = function(id, name, lowerBound, upperBound, children, parent) {
    var self = this;

    self.id = ko.observable(id);
    self.factoryName = ko.observable(name);
    self.lowerBound = ko.observable(lowerBound);
    self.upperBound = ko.observable(upperBound);
    self.childCount = ko.observable();
    self.children = children;
    self.parent = parent;
    self.errorMessages = ko.observableArray();

    self.childCount.extend({ required: { params: true, message: "Please enter a number of children to generate." }, number: true, min: 1, max: 15, step: 1 });
    self.upperBound.extend({ required: { params: true, message: "Please enter an upper bound." }, number: true, min: self.lowerBound, max: 2147483647, step: 1 });
    self.lowerBound.extend({ required: { params: true, message: "Please enter a lower bound." }, number: true, min: -2147483648, max: self.upperBound, step: 1 });
    self.factoryName.extend({ required: { params: true, message: "Please enter a name for the factory." }, maxLength: 50 });

    self.deleteNode = function() {
        parent.tree.server.deleteNode(self.id());
    };
    self.renameNode = function() {
        parent.tree.server.renameNode(self.id(), self.factoryName());
    };
    self.generateNode = function() {
        parent.tree.server.generateNode(self.id(), self.childCount()); //figure out where we're getting count from.
    };
    self.editNode = function() {
        parent.tree.server.editBounds(self.id(), self.lowerBound(), self.upperBound());
    };
    self.setAsSelected = function() {
        self.parent.setSelectedItem(self.id());
    };
    self.removeError = function(errorId) {
        self.errorMessages.remove(function(node) {
            return node.id() == errorId;
        });
    };
}