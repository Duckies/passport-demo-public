﻿using System;
using Newtonsoft.Json;

namespace PassportDemo.Models
{
    public class ErrorMessage
    {
        private readonly Guid? _factoryId;
        private readonly Guid _id;
        private readonly string _message;
        private DateTime _createDateTime;

        public ErrorMessage(Guid id, string message)
        {
            _id = Guid.NewGuid();
            _factoryId = id;
            _message = message;
            _createDateTime = DateTime.Now;
        }

        public ErrorMessage(string message)
        {
            _message = message;
        }

        public string GetSerializedMessage()
        {
            return
                JsonConvert.SerializeObject(
                    new
                    {
                        Id = _id,
                        FactoryId = _factoryId,
                        Message = _message,
                        TimeStamp = _createDateTime.ToShortTimeString()
                    });
        }
    }
}