﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PassportDemo.Models
{
    public class ChildNode : IValidatableObject
    {
        public ChildNode()
        {
            CreateDate = DateTime.Now;
        }

        [Key]
        public Guid Id { get; set; }

        [Required]
        public int? Value { get; set; }

        [Required]
        public Guid FactoryNodeId { get; set; }

        public DateTime CreateDate { get; set; }

        [Required]
        [ForeignKey("FactoryNodeId")]
        public virtual FactoryNode parent { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Value < parent.LowerBound || Value > parent.UpperBound)
                yield return new ValidationResult("Value must be between upper and lower bounds.", new[] {"Value"});

            if (parent == null)
                //This seems to be incorrect. Need to figure out how to do a getFactoryNode(id) in Entity.
                yield return new ValidationResult("Factory Node does not exist.", new[] {"parent", "FactoryNodeId"});
        }
    }
}