﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PassportDemo.Models
{
    public class FactoryNode : IValidatableObject
    {
        public FactoryNode()
        {
            CreateDate = DateTime.Now;
        }

        [Key]
        public Guid Id { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 1)]
        public string Name { get; set; }

        [Required]
        [Range(int.MinValue, int.MaxValue)]
        public int? LowerBound { get; set; }

        [Required]
        [Range(int.MinValue, int.MaxValue)]
        public int? UpperBound { get; set; }

        public DateTime CreateDate { get; set; }

        public virtual List<ChildNode> children { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (UpperBound < LowerBound)
                yield return
                    new ValidationResult("Upper bound must be greater than or equal to lower bound.",
                        new[] {"LowerBound", "UpperBound"});
            if (LowerBound.Equals(null))
                yield return new ValidationResult("Lower bound must not be blank.", new[] {"LowerBound"});
            if (UpperBound.Equals(null))
                yield return new ValidationResult("Upper bound must not be blank.", new[] {"UpperBound"});
        }
    }
}