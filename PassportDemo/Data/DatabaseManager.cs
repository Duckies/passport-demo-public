﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using PassportDemo.Models;

namespace PassportDemo.Data
{
    public class DatabaseManager
    {
        private readonly DatabaseContext context;

        public DatabaseManager(DatabaseContext context)
        {
            this.context = context;
        }

        public async Task<List<ChildNode>> GenerateChildNodes(Guid factoryNodeId, int count)
        {
            var seed = new Random();
            var fNode = context.Factories.First(x => x.Id.Equals(factoryNodeId));

            context.Children.RemoveRange(context.Children.Where(x => x.FactoryNodeId.Equals(factoryNodeId)));

            for (var i = 0; i < count; i++)
            {
                var node = new ChildNode
                {
                    FactoryNodeId = fNode.Id,
                    Id = Guid.NewGuid(),
                    Value = seed.Next(fNode.LowerBound.Value, fNode.UpperBound.Value),
                    parent = fNode
                };

                context.Children.Add(node);
            }

            await context.SaveChangesAsync();

            return context.Children.Where(x => x.FactoryNodeId.Equals(factoryNodeId)).ToList();
        }

        public async Task<List<ChildNode>> RealignChildNodes(Guid factoryNodeId)
        {
            var fNode = context.Factories.First(x => x.Id.Equals(factoryNodeId));

            var children =
                context.Children.Where(
                    x =>
                        x.FactoryNodeId.Equals(factoryNodeId) && x.Value > fNode.UpperBound ||
                        x.Value < fNode.LowerBound)
                    .ToList();

            children.ForEach(x => RealignChild(x, fNode));

            await context.SaveChangesAsync();

            return context.Children.Where(x => x.FactoryNodeId.Equals(factoryNodeId)).ToList();
        }

        public async Task<FactoryNode> AddFactoryNode(string name, int lowerBound, int upperBound)
        {
            var node = context.Factories.Add(new FactoryNode
            {
                Id = Guid.NewGuid(),
                Name = name,
                LowerBound = lowerBound,
                UpperBound = upperBound
            });

            await context.SaveChangesAsync(); //need to modify all save changes to be async.
            return node;
        }

        public async Task DeleteFactoryNode(Guid nodeId)
        {
            context.Factories.RemoveRange(context.Factories.Where(x => x.Id.Equals(nodeId)));
            context.Children.RemoveRange(context.Children.Where(x => x.FactoryNodeId.Equals(nodeId)));
            await context.SaveChangesAsync();
        }

        public void EditFactoryNodeBounds(Guid nodeId, int lowerBound, int upperBound)
        {
            var fNode = context.Factories.First(x => x.Id.Equals(nodeId));
            fNode.LowerBound = lowerBound;
            fNode.UpperBound = upperBound;
        }

        public async Task RenameFactoryNode(Guid nodeId, string name)
        {
            context.Factories.First(x => x.Id.Equals(nodeId)).Name = name;
            await context.SaveChangesAsync();
        }

        public List<FactoryNode> GetFactoryNodes()
        {
            return context.Factories.Include(lu => lu.children).ToList();
        }

        public bool FactoryNodeExist(Guid nodeId)
        {
            return context.Factories.Any(x => x.Id.Equals(nodeId));
        }

        private void RealignChild(ChildNode child, FactoryNode parent)
        {
            var seed = new Random();
            child.Value = seed.Next(parent.LowerBound.Value, parent.UpperBound.Value);
            child.parent = child.parent == null ? child.parent : parent;
        }
    }
}