﻿using System.Data.Entity;
using PassportDemo.Models;

namespace PassportDemo.Data
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext() : base("ConnectionStringForDatabase")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<DatabaseContext>());
        }

        public virtual DbSet<FactoryNode> Factories { get; set; }
        public virtual DbSet<ChildNode> Children { get; set; }
    }
}