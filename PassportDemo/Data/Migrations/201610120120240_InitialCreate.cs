using System.Data.Entity.Migrations;

namespace PassportDemo.Migrations
{
    public class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChildNodes",
                c => new
                {
                    Id = c.Guid(false),
                    Value = c.Int(false),
                    FactoryNodeId = c.Guid(false),
                    CreateDate = c.DateTime(false, defaultValueSql: "GETDATE()")
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FactoryNodes", t => t.FactoryNodeId, true)
                .Index(t => t.FactoryNodeId);

            CreateTable(
                "dbo.FactoryNodes",
                c => new
                {
                    Id = c.Guid(false),
                    Name = c.String(false, 255),
                    LowerBound = c.Int(false),
                    UpperBound = c.Int(false),
                    CreateDate = c.DateTime(false, defaultValueSql: "GETDATE()")
                })
                .PrimaryKey(t => t.Id);
        }

        public override void Down()
        {
            DropForeignKey("dbo.ChildNodes", "FactoryNodeId", "dbo.FactoryNodes");
            DropIndex("dbo.ChildNodes", new[] {"FactoryNodeId"});
            DropTable("dbo.FactoryNodes");
            DropTable("dbo.ChildNodes");
        }
    }
}