﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Newtonsoft.Json;
using PassportDemo.Controllers;
using PassportDemo.Data;
using PassportDemo.Models;

namespace PassportDemo.Hubs
{
    [HubName("treeHub")]
    public class TreeHub : Hub
    {
        private readonly ChildNodeController childController;
        private readonly FactoryNodeController factoryController;

        public TreeHub()
        {
            var context = new DatabaseContext();
            var manager = new DatabaseManager(context);
            factoryController = new FactoryNodeController(manager);
            childController = new ChildNodeController(manager);
        }

        public async Task AddNode(string name, int lowerBound, int upperBound)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(name))
                    Clients.Caller.throwException(new ErrorMessage("Name cannot be blank.").GetSerializedMessage());
                else if (lowerBound > upperBound)
                    Clients.Caller.throwException(
                        new ErrorMessage("Lower bound cannot be greater than upper bound.").GetSerializedMessage());
                else
                {
                    var node = await factoryController.AddNode(name, lowerBound, upperBound);
                    Clients.All.addNode(JsonConvert.SerializeObject(node));
                }
            }
            catch (Exception e)
            {
                Clients.Caller.throwException(
                    new ErrorMessage($"Unexpected error has occurred. {e.Message}").GetSerializedMessage());
            }
        }

        public async Task DeleteNode(Guid nodeId)
        {
            try
            {
                if (!factoryController.NodeExists(nodeId))
                    Clients.Caller.throwException(new ErrorMessage("Factory does not exist.").GetSerializedMessage());
                else
                {
                    await factoryController.DeleteNode(nodeId);
                    Clients.All.deleteNode(nodeId);
                }
            }
            catch (Exception e)
            {
                Clients.Caller.throwException(
                    new ErrorMessage(nodeId, $"Unexpected error has occurred. {e.Message}").GetSerializedMessage());
            }
        }

        public async Task RenameNode(Guid nodeId, string name)
        {
            try
            {
                if (!factoryController.NodeExists(nodeId))
                    Clients.Caller.throwException(new ErrorMessage("Factory does not exist.").GetSerializedMessage());
                else if (string.IsNullOrWhiteSpace(name))
                    Clients.Caller.throwException(
                        new ErrorMessage(nodeId, "Name cannot be blank.").GetSerializedMessage());
                else
                {
                    await factoryController.RenameNode(nodeId, name);
                    Clients.All.renameNode(nodeId, name);
                }
            }
            catch (Exception e)
            {
                Clients.Caller.throwException(
                    new ErrorMessage(nodeId, $"Unable to generate children at this time. {e.Message}")
                        .GetSerializedMessage());
            }
        }

        public async Task GenerateNode(Guid nodeId, int count)
        {
            try
            {
                if (!factoryController.NodeExists(nodeId))
                    Clients.Caller.throwException(new ErrorMessage("Factory does not exist.").GetSerializedMessage());
                else if (count <= 0)
                    Clients.Caller.throwException(
                        new ErrorMessage(nodeId, "Child count must be greater than 1.").GetSerializedMessage());
                else if (count > 15)
                    Clients.Caller.throwException(
                        new ErrorMessage(nodeId, "Child count cannot be greater than 15.").GetSerializedMessage());
                else
                {
                    var generatedChildren = await childController.GenerateChildren(nodeId, count);

                    var serializedChildren = JsonConvert.SerializeObject(
                        from child in generatedChildren
                        select new
                        {
                            child.Id,
                            child.Value,
                            child.FactoryNodeId
                        });

                    Clients.All.generateNode(nodeId, serializedChildren);
                }
            }
            catch (Exception e)
            {
                Clients.Caller.throwException(
                    new ErrorMessage(nodeId, $"Unable to generate children at this time. {e.Message}")
                        .GetSerializedMessage());
            }
        }

        public async Task EditBounds(Guid nodeId, int lowerBound, int upperBound)
        {
            try
            {
                if (!factoryController.NodeExists(nodeId))
                    Clients.Caller.throwException(new ErrorMessage("Factory does not exist.").GetSerializedMessage());
                else if (lowerBound > upperBound)
                    Clients.Caller.throwException(
                        new ErrorMessage(nodeId, "Lower bound cannot be greater than upper bound.").GetSerializedMessage
                            ());
                else
                {
                    factoryController.EditNodeBounds(nodeId, lowerBound, upperBound);

                    var children = await childController.RealignChildren(nodeId, lowerBound, upperBound);

                    var serializedChildren = JsonConvert.SerializeObject(
                        from child in children
                        select new
                        {
                            child.Id,
                            child.Value,
                            child.FactoryNodeId
                        });

                    Clients.All.editBounds(nodeId, lowerBound, upperBound, serializedChildren);
                }
            }
            catch (Exception e)
            {
                Clients.Caller.throwException(
                    new ErrorMessage(nodeId, $"Unable to generate children at this time. {e.Message}")
                        .GetSerializedMessage());
            }
        }

        public void GetNext20(Guid? guid = null)
        {
            var list = factoryController.GetState();
            var factories = from factory in list
                select new
                {
                    factory.Id,
                    factory.Name,
                    factory.LowerBound,
                    factory.UpperBound,
                    factory.CreateDate,
                    Children = from child in factory.children
                        select new
                        {
                            child.Id,
                            child.Value
                        }
                }
                into selection
                orderby selection.CreateDate ascending
                select selection;


            var factoryList = factories.ToList();
            var startIndex = guid.Equals(null) ? 0 : factoryList.IndexOf(factoryList.First(x => x.Id.Equals(guid))) + 1;
            var endIndex = factoryList.Count >= startIndex + 20 ? 20 : factoryList.Count - startIndex;

            factoryList = factoryList.GetRange(startIndex, endIndex);

            Clients.Caller.getState(JsonConvert.SerializeObject(factoryList));
        }

        public void GetInitialLoad()
        {
            try
            {
                var list = factoryController.GetState();
                var factories = from factory in list
                    select new
                    {
                        factory.Id,
                        factory.Name,
                        factory.LowerBound,
                        factory.UpperBound,
                        factory.CreateDate,
                        Children = from child in factory.children
                            select new
                            {
                                child.Id,
                                child.Value
                            }
                    }
                    into selection
                    orderby selection.CreateDate ascending
                    select selection;

                var factoryList = factories.ToList().GetRange(0, factories.Count() > 20 ? 20 : factories.Count());

                Clients.Caller.getState(JsonConvert.SerializeObject(factoryList));
            }
            catch (Exception e)
            {
                Clients.Caller.throwException(
                    new ErrorMessage($"Unexpected error has occurred. {e.Message}").GetSerializedMessage());
            }
        }
    }
}