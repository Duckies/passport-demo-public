﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PassportDemo.Data;
using PassportDemo.Models;

namespace PassportDemo.Controllers
{
    public class ChildNodeController
    {
        private readonly DatabaseManager manager;

        public ChildNodeController(DatabaseManager manager)
        {
            this.manager = manager;
        }

        public async Task<List<ChildNode>> GenerateChildren(Guid factoryNodeId, int count)
        {
            return await manager.GenerateChildNodes(factoryNodeId, count);
        }

        public async Task<List<ChildNode>> RealignChildren(Guid factoryNodeId, int lowerBound, int upperBound)
        {
            return await manager.RealignChildNodes(factoryNodeId);
        }
    }
}