﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PassportDemo.Data;
using PassportDemo.Models;

namespace PassportDemo.Controllers
{
    public class FactoryNodeController
    {
        private readonly DatabaseManager manager;

        public FactoryNodeController(DatabaseManager manager)
        {
            this.manager = manager;
        }

        public async Task<FactoryNode> AddNode(string name, int lowerBound, int upperBound)
        {
            return await manager.AddFactoryNode(name, lowerBound, upperBound);
        }

        public async Task DeleteNode(Guid nodeId)
        {
            await manager.DeleteFactoryNode(nodeId);
        }

        public void EditNodeBounds(Guid nodeId, int lowerBound, int upperBound)
        {
            manager.EditFactoryNodeBounds(nodeId, lowerBound, upperBound);
        }

        public async Task RenameNode(Guid nodeId, string name)
        {
            await manager.RenameFactoryNode(nodeId, name);
        }

        public List<FactoryNode> GetState()
        {
            return manager.GetFactoryNodes();
        }

        public bool NodeExists(Guid nodeId)
        {
            return manager.FactoryNodeExist(nodeId);
        }
    }
}