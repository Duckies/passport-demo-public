﻿var getChildNodes = function(childjson) {
    var children = ko.observableArray();

    if (childjson != null) {
        childjson.forEach(function(child) {
            children.push(new childViewModel(child.Id, child.Value, child.FactoryNodeId));
        });
    }

    return children;
};
var replaceChildren = function(node, children) {
    node.children.removeAll();

    children().forEach(function(kid) {
        node.children.push(kid);
    });
};