﻿var childViewModel = function(id, value, factoryNodeId) {
    this.id = ko.observable(id);
    this.value = ko.observable(value);
    this.factoryNodeId = ko.observable(factoryNodeId);
}