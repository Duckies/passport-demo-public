﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using PassportDemo.Controllers;
using PassportDemo.Data;
using PassportDemo.Models;

namespace PassportDemo.Tests.Controllers
{
    [TestFixture]
    class ChildNodeControllerTest
    {
        private Mock<DbSet<FactoryNode>> mockSetFactory;
        private Mock<DbSet<ChildNode>> mockSetChild;
        private Mock<DatabaseContext> mockContext;
        private Mock<DatabaseManager> mockDatabaseManager;
        private ChildNodeController controller;
        private FactoryNode node;
        private ChildNode child;
        [OneTimeSetUp]
        public void SetupFixture()
        {
            var guid = new Guid();

            node = new FactoryNode()
            {
                Id = guid,
                LowerBound = 0,
                UpperBound = 5,
                Name = "Test Object"
            };

            child = new ChildNode()
            {
                Id = guid,
                FactoryNodeId = Guid.NewGuid(),
                parent = node,
                Value = 5
            };

            mockSetFactory = new Mock<DbSet<FactoryNode>>();
            mockSetChild = new Mock<DbSet<ChildNode>>();
            mockContext = new Mock<DatabaseContext>();
            mockDatabaseManager = new Mock<DatabaseManager>(mockContext.Object);
            mockContext.Setup(x => x.Factories).Returns(mockSetFactory.Object);
            mockContext.Setup(x => x.Children).Returns(mockSetChild.Object);
            mockSetFactory.Setup(x => x.Add(It.IsAny<FactoryNode>())).Returns(node);
            mockSetChild.Setup(x => x.Add(It.IsAny<ChildNode>())).Returns(child);

            controller = new ChildNodeController(mockDatabaseManager.Object);

            var testDataFactory = new List<FactoryNode> { node }.AsQueryable();
            mockSetFactory.As<IQueryable<FactoryNode>>().Setup(m => m.Provider).Returns(testDataFactory.Provider);
            mockSetFactory.As<IQueryable<FactoryNode>>().Setup(m => m.Expression).Returns(testDataFactory.Expression);
            mockSetFactory.As<IQueryable<FactoryNode>>().Setup(m => m.ElementType).Returns(testDataFactory.ElementType);
            mockSetFactory.As<IQueryable<FactoryNode>>().Setup(m => m.GetEnumerator()).Returns(testDataFactory.GetEnumerator);

            var testDataChild = new List<ChildNode> { child }.AsQueryable();
            mockSetChild.As<IQueryable<ChildNode>>().Setup(m => m.Provider).Returns(testDataChild.Provider);
            mockSetChild.As<IQueryable<ChildNode>>().Setup(m => m.Expression).Returns(testDataChild.Expression);
            mockSetChild.As<IQueryable<ChildNode>>().Setup(m => m.ElementType).Returns(testDataChild.ElementType);
            mockSetChild.As<IQueryable<ChildNode>>().Setup(m => m.GetEnumerator()).Returns(testDataChild.GetEnumerator);
        }

        [TearDown]
        public void TearDown()
        {
            mockSetFactory.ResetCalls();
            mockSetChild.ResetCalls();
            mockContext.ResetCalls();
        }

        [Test]
        public void GenerateChildrenSuccessfulWithValidFactoryIdAndCount()
        {
            var children = controller.GenerateChildren(node.Id, 1);

            mockSetChild.Verify(x => x.Add(It.IsAny<ChildNode>()), Times.Once);
            mockContext.Verify(x => x.SaveChangesAsync(), Times.Once);
        }

        [Test]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        [TestCase(5)]
        [TestCase(10)]
        [TestCase(15)]
        public void GenerateChildrenCallsAddForEachChild(int count)
        {
            var children = controller.GenerateChildren(node.Id, count);

            mockSetChild.Verify(x => x.Add(It.IsAny<ChildNode>()), Times.Exactly(count));
            mockContext.Verify(x => x.SaveChangesAsync(), Times.Once);
        }
    }
}
