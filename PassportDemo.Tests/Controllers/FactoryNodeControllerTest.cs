﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using PassportDemo.Models;
using Moq;
using NUnit.Framework;
using PassportDemo.Controllers;
using PassportDemo.Data;

namespace PassportDemo.Tests.Controllers
{
    [TestFixture]
    public class FactoryNodeControllerTest
    {
        private Mock<DbSet<FactoryNode>> mockSetFactory;
        private Mock<DbSet<ChildNode>> mockSetChild;
        private Mock<DatabaseContext> mockContext;
        private Mock<DatabaseManager> mockDatabaseManager;
        private FactoryNodeController controller;

        [OneTimeSetUp]
        public void SetupFixture()
        {
            var guid = new Guid();
            var node = new FactoryNode()
            {
                Id = guid,
                LowerBound = 0,
                UpperBound = 5,
                Name = "Test Object"
            };
            var child = new ChildNode()
            {
                Id = guid,
                FactoryNodeId = Guid.NewGuid(),
                parent = node,
                Value = 5
            };

            mockSetFactory = new Mock<DbSet<FactoryNode>>();
            mockSetChild = new Mock<DbSet<ChildNode>>();
            mockContext = new Mock<DatabaseContext>();
            mockDatabaseManager = new Mock<DatabaseManager>(mockContext.Object);
            mockContext.Setup(x => x.Factories).Returns(mockSetFactory.Object);
            mockContext.Setup(x => x.Children).Returns(mockSetChild.Object);
            mockSetFactory.Setup(x => x.Add(It.IsAny<FactoryNode>())).Returns(node);
            mockSetChild.Setup(x => x.Add(It.IsAny<ChildNode>())).Returns(child);

            controller = new FactoryNodeController(mockDatabaseManager.Object);

            var testDataFactory = new List<FactoryNode> { node }.AsQueryable();
            mockSetFactory.As<IQueryable<FactoryNode>>().Setup(m => m.Provider).Returns(testDataFactory.Provider);
            mockSetFactory.As<IQueryable<FactoryNode>>().Setup(m => m.Expression).Returns(testDataFactory.Expression);
            mockSetFactory.As<IQueryable<FactoryNode>>().Setup(m => m.ElementType).Returns(testDataFactory.ElementType);
            mockSetFactory.As<IQueryable<FactoryNode>>().Setup(m => m.GetEnumerator()).Returns(testDataFactory.GetEnumerator);

            var testDataChild = new List<ChildNode> { child }.AsQueryable();
            mockSetChild.As<IQueryable<ChildNode>>().Setup(m => m.Provider).Returns(testDataChild.Provider);
            mockSetChild.As<IQueryable<ChildNode>>().Setup(m => m.Expression).Returns(testDataChild.Expression);
            mockSetChild.As<IQueryable<ChildNode>>().Setup(m => m.ElementType).Returns(testDataChild.ElementType);
            mockSetChild.As<IQueryable<ChildNode>>().Setup(m => m.GetEnumerator()).Returns(testDataChild.GetEnumerator);
        }

        [TearDown]
        public void TearDown()
        {
            mockSetFactory.ResetCalls();
            mockSetChild.ResetCalls();
            mockContext.ResetCalls();
        }

        [Test]
        public void AddNodeSavesViaContext()
        {
            var node = controller.AddNode("Successfully Saves", 0, 10).Result;

            mockSetFactory.Verify(x => x.Add(It.IsAny<FactoryNode>()), Times.Once);
            mockContext.Verify(x => x.SaveChangesAsync(), Times.Once);
        }

        [Test]
        [TestCase(int.MinValue, int.MaxValue)]
        [TestCase(0, 0)]
        [TestCase(0, 1)]
        [TestCase(-1, 0)]
        [TestCase(-1, 1)]
        public void AddNodeSucceedsWithValidatingBounds(int lower, int upper)
        {
            var node = controller.AddNode("Successfully Saves", lower, upper).Result;

            mockSetFactory.Verify(x => x.Add(It.IsAny<FactoryNode>()), Times.Once);
            mockContext.Verify(x => x.SaveChangesAsync(), Times.Once);
        }

        [Test]
        public void DeleteSuccessfullyCallsEntityTwice()
        {
            var added = controller.AddNode("Test", 1, 10).Result;

            controller.DeleteNode(added.Id);

            mockSetFactory.Verify(x => x.RemoveRange(It.IsAny<IEnumerable<FactoryNode>>()), Times.Once);
            mockSetChild.Verify(x => x.RemoveRange(It.IsAny<IEnumerable<ChildNode>>()), Times.Once);
            mockContext.Verify(x => x.SaveChangesAsync(), Times.Exactly(2));
        }
    }
}
