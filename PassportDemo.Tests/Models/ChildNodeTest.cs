﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using PassportDemo.Models;
using NUnit.Framework;
using Moq;
using PassportDemo.Controllers;
using Assert = NUnit.Framework.Assert;

namespace PassportDemo.Tests.Models
{
    [TestFixture]
    class ChildNodeTest
    {
        public FactoryNode factory;
        public ChildNode child;

        [SetUp]
        public void SetUp()
        {
            factory = new FactoryNode
            {
                LowerBound = -10,
                UpperBound = 10,
                Name = "Factory Name",
                Id = new Guid()
            };

            child = new ChildNode
            {
                Id = new Guid(),
                FactoryNodeId = factory.Id,
                parent = factory,
                Value = 0
            };
        }

        [Test]
        public void ValueIsBetweenFactoryNodeBoundsAndIsValid()
        {
            var validationContext = new ValidationContext(child, null, null);
            var results = new List<ValidationResult>();
            var actual = Validator.TryValidateObject(child, validationContext, results);

            Assert.AreEqual(results.Count, 0);
            Assert.AreEqual(true, actual);
        }

        [Test]
        public void ValueIsNotBetwenFactoryNodeBoundsAndIsInvalid()
        {
            child.Value = -100;

            var validationContext = new ValidationContext(child, null, null);
            var results = new List<ValidationResult>();
            var actual = Validator.TryValidateObject(child, validationContext, results);

            Assert.AreEqual(results.Count, 1);
            Assert.AreEqual(results[0].ErrorMessage, "Value must be between upper and lower bounds.");
            Assert.AreEqual(results[0].MemberNames, new[] { "Value" });
            Assert.AreEqual(false, actual);
        }

        [Test]
        public void ChildNodeHasNoParentAndIsInvalid()
        {
            child.parent = null;
            child.FactoryNodeId = Guid.Empty;

            var validationContext = new ValidationContext(child, null, null);
            var results = new List<ValidationResult>();
            var actual = Validator.TryValidateObject(child, validationContext, results);

            Assert.AreEqual(results.Count, 1);
            Assert.AreEqual(results[0].ErrorMessage, "The parent field is required.");
            Assert.AreEqual(results[0].MemberNames, new[] { "parent" });
            Assert.AreEqual(false, actual);
        }
    }
}
