﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using NUnit.Framework;
using PassportDemo.Models;
using Assert = NUnit.Framework.Assert;

namespace PassportDemo.Tests.Models
{
    [TestFixture]
    class FactoryNodeTest
    {
        [Test]
        [TestCase(int.MinValue, int.MaxValue)]
        [TestCase(0, 1)]
        [TestCase(-1, 0)]
        [TestCase(-1, 1)]
        [TestCase(0, 0)]
        public void FactorySavesWithValidBounds(int lower, int upper)
        {
            var factory = new FactoryNode();
            factory.LowerBound = lower;
            factory.UpperBound = upper;
            factory.Name = "Factory Name";

            var validationContext = new ValidationContext(factory, null, null);
            var results = new List<ValidationResult>();
            var actual = Validator.TryValidateObject(factory, validationContext, results);

            Assert.AreEqual(results.Count, 0);
            Assert.AreEqual(true, actual);
        }

        [Test]
        [TestCase(int.MaxValue, int.MinValue)]
        [TestCase(-1, -2)]
        [TestCase(1, -1)]
        [TestCase(0, -2)]
        [TestCase(0, -1)]
        [TestCase(1, 0)]
        public void FactorySaveFailsWithInvalidBounds(int lower, int upper)
        {
            var factory = new FactoryNode();
            factory.LowerBound = lower;
            factory.UpperBound = upper;
            factory.Name = "Factory Name";

            var validationContext = new ValidationContext(factory, null, null);
            var results = new List<ValidationResult>();
            var actual = Validator.TryValidateObject(factory, validationContext, results);

            Assert.AreEqual(results.Count, 1);
            Assert.AreEqual(results[0].ErrorMessage, "Upper bound must be greater than or equal to lower bound.");
            Assert.AreEqual(results[0].MemberNames, new[] { "LowerBound", "UpperBound" });
            Assert.AreEqual(false, actual);
        }

        [Test]
        [TestCase("Test")]
        public void FactorySavesWithValidName(string name)
        {
            var factory = new FactoryNode();
            factory.Name = name;
            factory.LowerBound = -1;
            factory.UpperBound = 1;

            var validationContext = new ValidationContext(factory, null, null);
            var results = new List<ValidationResult>();
            var actual = Validator.TryValidateObject(factory, validationContext, results);

            Assert.AreEqual(results.Count, 0);
            Assert.AreEqual(true, actual);
        }

        [Test]
        [TestCase("")]
        public void FactorySaveFailsWithInvalidName(string name)
        {
            var factory = new FactoryNode();
            factory.Name = string.Empty;
            factory.LowerBound = -1;
            factory.UpperBound = 1;
            var validationContext = new ValidationContext(factory, null, null);
            var results = new List<ValidationResult>();
            var actual = Validator.TryValidateObject(factory, validationContext, results);

            Assert.AreEqual(results.Count, 1);
            Assert.AreEqual(results[0].ErrorMessage, "The Name field is required.");
            Assert.AreEqual(results[0].MemberNames, new[] { "Name" });
            Assert.AreEqual(false, actual);
        }
    }
}
